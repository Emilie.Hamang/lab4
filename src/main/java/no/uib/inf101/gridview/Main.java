package no.uib.inf101.gridview;

import java.awt.Color;

import javax.swing.JFrame;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;

public class Main {
  public static void main(String[] args) {
    // TODO: Implement this method
    GridView canvas;
    JFrame frame = new JFrame();
    ColorGrid theGrid = new ColorGrid(3, 4);

    // setter fargene i hjørnene
    theGrid.set(new CellPosition(0, 0), Color.RED);
    theGrid.set(new CellPosition(0, 3), Color.BLUE);
    theGrid.set(new CellPosition(2, 0), Color.YELLOW);
    theGrid.set(new CellPosition(2, 3), Color.GREEN);

    canvas = new GridView(theGrid);
    // oppretter tegningen
    frame.setContentPane(canvas);
    frame.setTitle("hello");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);

  }
}
