package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel {
  // TODO: Implement this class
  public IColorGrid colorGrid;
  public CellPosition position;
  private static final double OUTER_MARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid colorGrid) {
    this.colorGrid = colorGrid;
    this.setPreferredSize(new Dimension(400, 300));
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    // her må jeg kalle på drawGrid
    drawGrid(g2);

  }

  // metode som har Graphics 2D objekt som parameter og ingen returvardi
  // draw grid skal kalle på draw cells
  private void drawGrid(Graphics2D g2) {
    double x = OUTER_MARGIN;
    double y = OUTER_MARGIN;
    double width = this.getWidth() - 2 * x;
    double heigth = this.getHeight() - 2 * y;
    g2.setColor(MARGINCOLOR);
    Rectangle2D rectangle = new Rectangle2D.Double(x, y, width, heigth);
    g2.fill(rectangle);

    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(rectangle, colorGrid, OUTER_MARGIN);
    drawCells(g2, colorGrid, converter);
  }

  // hjelpemetode drawCells
  private static void drawCells(Graphics2D g2, CellColorCollection color, CellPositionToPixelConverter converter) {
    for (CellColor cellColor : color.getCells()) {
      Rectangle2D rectangleDraw = converter.getBoundsForCell(cellColor.cellPosition());
      if (cellColor.color() == null) {
        g2.setColor(Color.DARK_GRAY);
      } else {
        g2.setColor(cellColor.color());
      }
      g2.fill(rectangleDraw);
    }
  }
}
