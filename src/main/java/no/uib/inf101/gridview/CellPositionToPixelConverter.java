package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
  // TODO: Implement this class
  Rectangle2D box;
  GridDimension gd;
  Double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  // returnerer en type Rectangle2D
  public Rectangle2D getBoundsForCell(CellPosition cp) {
    // må regne ut x, y, bredde, høyde for en rute
    // deretter retrnere et nytt Rectangle2D.Double objekt
    double x = this.box.getX();
    double y = this.box.getY();
    double width = this.box.getWidth();
    double heigth = this.box.getHeight();
    double col = cp.col();
    double row = cp.row();
    double cols = this.gd.cols();
    double rows = this.gd.rows();
    double margin = this.margin;

    // finne lengden på cellene
    double cellWidth = (width - margin * (cols + 1)) / cols;
    // finne høyden på cellene
    double cellHeigth = (heigth - margin * (rows + 1)) / rows;
    // finne cellX ()
    double cellX = x + col * cellWidth + margin * (col + 1);
    // finne cellY
    double cellY = y + row * cellHeigth + margin * (row + 1);

    return new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeigth);
  }
}
