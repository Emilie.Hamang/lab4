package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
  // TODO: Implement this class
  int row;
  int col;
  ArrayList<CellColor> grid;

  public ColorGrid(int row, int col) {
    this.row = row;
    this.col = col;
    this.grid = new ArrayList<>();
    for (int i = 0; i < row; i += 1) {
      for (int j = 0; j < col; j += 1) {
        grid.add(new CellColor(new CellPosition(i, j), null));
      }
    }
  }

  @Override
  public Color get(CellPosition pos) {
    int row = pos.row();
    int col = pos.col();
    if (col > this.col - 1 || row > this.row - 1) {
      throw new IndexOutOfBoundsException("Out of bounds");
    } else if (col < 0 || row < 0) {
      throw new IndexOutOfBoundsException("Out of bounds");
    }
    for (CellColor object : grid) {
      if (object.cellPosition().equals(pos)) {
        return object.color();
      }
    }
    return null;
  }

  @Override
  public void set(CellPosition pos, Color color) {
    int col = pos.col();
    int row = pos.row();
    if (col > this.col - 1 || row > this.row - 1) {
      throw new IndexOutOfBoundsException("Out of bounds");
    } else if (col < 0 || row < 0) {
      throw new IndexOutOfBoundsException("Out of bounds");
    } else {
      CellColor newCell = new CellColor(pos, color);
      int index = 0;
      for (CellColor object : grid) {
        if (object.cellPosition().equals(pos)) {
          grid.set(index, newCell);
          break;
        }
        index++;
      }
    }

  }

  @Override
  public int rows() {
    return this.row;
  }

  @Override
  public int cols() {
    return this.col;
  }

  @Override
  public List<CellColor> getCells() {
    return this.grid;
  }
}
